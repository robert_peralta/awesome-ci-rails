# frozen_string_literal: true
require 'rails_helper'

RSpec.describe HomeController, type: :controller do

	context 'GET index' do
		it 'should assign server_object to instance' do
			get :index
			expect(assigns(:server_object)).to include(:foo)
		end
	end

end
