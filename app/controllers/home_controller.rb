# frozen_string_literal: true
class HomeController < ApplicationController
	def index
		@server_object = {foo: 'bar'}
	end
end